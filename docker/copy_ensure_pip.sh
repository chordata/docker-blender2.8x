#!/bin/bash

if [ -z "$1" ]; then
	echo "Usage: copy_ensure_pip.sh <destination_dir>"
	exit 1
fi

ENSUREPIP_PATH=$(python3 -c 'import re; import ensurepip; m=re.match(r"^/.*/ensurepip", ensurepip.__file__); print(m.group(0))')

if [ "$?" != "0" ]; then
	echo Could not found module 'ensurepip'. Aborting.
	exit 1
fi

cp -r $ENSUREPIP_PATH $1
