# Debian + Blender 2.8x docker image 
Based on [Egidijus Ligeika's docker env for dockerizing blender](https://github.com/egidijus/blender-compile)

This forks uses [multistage build](https://docs.docker.com/develop/develop-images/multistage-build/) to create a minimized Debian version with just the dependencies required to run Blender

## How

Install `make` and `git` on your machine and clone this repo.
Then, checkout the blender code:
```
make clone
```

Then, build the debian docker image with all the dependencies for blender.
```
make build
```

Then, you should be able to run the container and have the blender compiled:
```
make up
```

